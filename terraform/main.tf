provider "azurerm" {
  features {}
}

resource "azurerm_resource_group" "puppet-terraform-integration-rg" {
  name     = "puppet-terraform-integration-rg"
  location = "East US"
}

resource "azurerm_virtual_network" "example" {
  name                = "example-network"
  resource_group_name = azurerm_resource_group.puppet-terraform-integration-rg.name
  location            = azurerm_resource_group.puppet-terraform-integration-rg.location
  address_space       = ["10.0.0.0/16"]
}

resource "azurerm_subnet" "example" {
  name                 = "example-subnet"
  resource_group_name  = azurerm_resource_group.puppet-terraform-integration-rg.name
  virtual_network_name = azurerm_virtual_network.example.name
  address_prefixes     = ["10.0.1.0/24"]
}

resource "azurerm_network_interface" "example" {
  name                = "example-nic"
  location            = azurerm_resource_group.puppet-terraform-integration-rg.location
  resource_group_name = azurerm_resource_group.puppet-terraform-integration-rg.name

  ip_configuration {
    name                          = "internal"
    subnet_id                     = azurerm_subnet.example.id
    private_ip_address_allocation = "Dynamic"
  }
}

resource "azurerm_linux_virtual_machine" "example" {
  name                = "puppet-terraform-vm"
  resource_group_name = azurerm_resource_group.puppet-terraform-integration-rg.name
  location            = azurerm_resource_group.puppet-terraform-integration-rg.location
  size                = "Standard_B1s"

  admin_username = "dhanush"
  network_interface_ids = [azurerm_network_interface.example.id]

  admin_ssh_key {
    username   = "dhanush"
    public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDS1e+WmptGQ23q5902iuAg9akDGZzzEuMHJ1xWJE73IXI7fQRkUdPEIupOxlpBrH09vUsB7AKjXr81/Ha1B9Jf2dWaJVaZf+MMr3V/VgQZodrfiUiX5fzD6ekO4hUPXM2+8jkyLd63PJH45UAgLmVEelvbeOeZm2YN2DURLKOkKe0GP06dmIY75xRjQ5+y6xi5JQ5cTl7r9WGVi4QV3g9Q4POeTcz+EW/RdlQ82i02QPDuHgS3kb3yhJRALhEGZD5fnnhIuH5d7GOb9IxBHrVrfSAZwxHbu2VQL/sl0cFIoVNEs9Q3xUaMp60YleSela0tJt5F6nCSTTnJVUZGZFG1 dell@DESKTOP-R3O90SV"
  }

  os_disk {
    caching              = "ReadWrite"
    storage_account_type = "Standard_LRS"
  }

  source_image_reference {
    publisher = "Canonical"
    offer     = "UbuntuServer"
    sku       = "18.04-LTS"
    version   = "latest"
  }

  computer_name  = "puppet-terraform-vm"
}
